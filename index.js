const express=require("express")
const app=express()
const mongoose=require("mongoose")
const expressSession=require("express-session")
const cookieParser=require("cookie-parser")
const connectFlash=require("connect-flash")

app.set("view engine","ejs")

app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.use(cookieParser("secret_password"))
app.use(expressSession({
    secret:"secret_password",
    cookie:{maxAge:4000000},
    saveUninitialized:false,
    resave:false
}))
app.use(connectFlash())
const controller=require("./controllers/routecontroller")
app.use((req,res,next)=>{
    res.locals.flashMessages=req.flash()
    next()
})

app.get("/",controller.home)
app.get("/form",controller.form)
app.post("/form",controller.formPost)
app.get("/thanks",controller.thanks)

const PORT=process.env.PORT||3000
app.listen(PORT,()=>{
    console.log(`Server starting on port ${PORT}`)
})