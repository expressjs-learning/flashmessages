module.exports={
    home:(req,res)=>{
        res.render("home")
    },
    form:(req,res)=>{
        res.render("form")
    },
    thanks:(req,res)=>{
        res.render("thanks")
    },
    formPost:(req,res)=>{
        let {password,confirmPassword}=req.body
        if(password==confirmPassword){
            req.flash("success","password correct")
            res.redirect("/thanks")
        }else{
            req.flash("failure","error password not similar")
            res.redirect("/form")
        }
    }
}